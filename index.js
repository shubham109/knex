const csv = require('csvjson')
const fs = require('fs')
const knex = require("knex")({
	client: "oracledb",
	connection: {
		user: "XXXXXXXXX",
		password: "XXXXXXXXX",
		connectString: "XXXXXXXXXXXXX",
		database:"XXXXXXXXXXXXX"
		}
});
const file = fs.readFileSync('data.csv', 'utf8');

const dataObj = csv.toObject(file)
//console.log(dataObj)

                                             /*  Insert */
/*Creates an insert query, taking either a hash of properties to be inserted into the row, or an array of inserts, to be executed as a single insert command. If returning array is passed e.g. ['id', 'title'], it resolves the promise / fulfills the callback with an array of all the added rows with specified columns. It's a shortcut for returning method*/
//for small dataset you should use insert()
knex.insert(dataObj).into('DATA').then(() => {
        console.log("success")
        process.exit(0)
    })
    .catch((err) => {
    	console.log(err)
    })
                                                            /*BatchInsert */

    /* The batchInsert utility will insert a batch of rows wrapped inside a transaction (which is automatically created unless explicitly given a transaction using transacting), at a given chunkSize.

It's primarily designed to be used when you have thousands of rows to insert into a table.

By default, the chunkSize is set to 1000.*/

//for large dataset you should use batchinsert
knex.batchInsert('DATA',dataObj,1000)
.returning('CITY')                  //returning Particular column
.then((REGION) => {                
	console.log(REGION)
})
.catch((err) => {
	console.log(err)
})